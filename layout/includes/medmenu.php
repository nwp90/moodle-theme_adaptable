<!-- BRENDONS MENU STARTS HERE ---------------------->          
<div id="brnav-container">

  <!-- HELP Menu -->  
  <div id="helpmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Help Navigation</h2></li>
            <li class="home">
        <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#helpmenu">Close X</button>
      </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  
            <ul class="unstyled span4 mylist">
              <li class="programme">Student Resources</li>
              <li><a title="Moodle Support" href="https://medschool.otago.ac.nz/course/view.php?id=214">Moodle Support</a></li>
              <li><a title="Student Support" href="https://medschool.otago.ac.nz/course/view.php?id=1309">Student Support</a></li>
              <li><a title="MB ChB Policies and Guidelines" href="https://www.otago.ac.nz/medicine/current-students/resources/policies-guides/index.html">MB ChB Policies and Guidelines</a></li>
          </ul> 

          <ul class="unstyled span4 mylist"> 
            <li class="programme">Staff Resources</li>
            <li><a title="Moodle Help Site" href="https://help.otago.ac.nz/moodle/">Moodle Help Site</a></li>
            <li><a title="Teaching and Learning in Health Sciences" href="http://learning.healthsci.otago.ac.nz">Teaching and Learning in Health Sciences</a></li>
            <li><a title="Examples Course" href="https://medschool.otago.ac.nz/course/view.php?id=479">Examples Course</a></li>
            <li><a title="ELM Tutor Resources" href="https://medschool.otago.ac.nz/course/view.php?id=224">ELM Tutor Resources</a></li>
          </ul>

          <ul class="unstyled span4 mylist">
            <li class="programme">Support</li>
            <li><a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a></li>
            <li><br /></li>
            <li class="programme">Other Moodles</li>
            <li><a title="For Non-Medical Programme Health Sciences Courses" href="https://hsmoodle.otago.ac.nz">HS Moodle</a></li>
            <li><a title="For Dept of Anatomy Courses" href="https://anatomystudent.otago.ac.nz">Anatomy Moodle</a></li>
            <li><a title="For Medical Progarmme Examinations" href="https://medtest.otago.ac.nz">MedTest Moodle</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <!-- old STAFF Menu   
  <div id="staffmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Staff Support Navigation</h2></li>
            <li class="home">
        <a title="Staff Teaching and Learning Support" href="https://medschool.otago.ac.nz/course/view.php?id=224">Staff Teaching and Learning Support</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#staffmenu">Close X</button>
      </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span4 mylist">
            <li class="programme">Moodle Resources</li>
            <li><a title="Moodle Staff Support" href="https://medschool.otago.ac.nz/course/view.php?id=143">Moodle Staff Support</a></li>
            <li><a title="Moodle Handbook" href="http://facmed.otago.ac.nz/elearning/wp-content/uploads/sites/6/2010/12/using_moodle_2e.pdf">Moodle Handbook for staff</a></li>
            <li><a title="Video Tutorials for staff" href="https://medschool.otago.ac.nz/course/view.php?id=143#section-2">Video Tutorials for staff</a></li>
          </ul> 

          <ul class="unstyled span4 mylist"> 
            <li class="programme">eLearning Resources</li>
            <li><a title="Examples Course" href="https://medschool.otago.ac.nz/course/view.php?id=479">Examples Course</a></li>
            <li><a title="How To" href="http://elearning.healthsci.otago.ac.nz">How To</a></li>
          </ul>

          <ul class="unstyled span4 mylist">
            <li class="programme">Support</li>
            <li>
        <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
      </li>
            <li><a title="PostGrad" href="https://hsmoodle.otago.ac.nz">PostGrad</a></li>
          </ul>

        </div>
      </li>
      
    </ul>  
  </div>
  -->

  <!-- ELM 2 Menu -->  
  <div id="elm2" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 2 Navigation</h2></li>
            <li class="home"><a title="ELM 2 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1638">ELM 2 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm2">Close X</button>
        </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 2 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (Block)" href="https://medschool.otago.ac.nz/course/view.php?id=1645">Psychological Medicine (Block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1630">Cardiovascular System</a></li>
            <li><a title="Gastrointestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1636">Gastrointestinal System</a></li>
            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1640">Musculoskeletal System</a></li>
            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1648">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 2 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1627">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1628">Cancer</a></li>
            <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1642">Clinical Pharmacology</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1631">EBP</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1632">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1634">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1635">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1637">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1639">Infection &amp; Immunology</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1641">Pathology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1643">Professional Development</a></li>
            <li><a title="Psychological Medicine (Vertical)" href="https://medschool.otago.ac.nz/course/view.php?id=1644">Psychological Medicine (Vertical)</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1646">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1649">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1633">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1629">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2018 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1663">2018 HSFY</a></li>
            <li><a title="2019 ResearchSmart" href="https://medschool.otago.ac.nz/course/view.php?id=1647">2019 ResearchSmart</a></li>
            <li><a title="MBChB Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">MBChB Resources</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>

  <!-- ELM 3 Menu -->  
  <div id="elm3" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 3 Navigation</h2></li>
            <li class="home"><a title="ELM 3 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1427">ELM 3 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm3">Close X</button>
        </li>
          <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 3 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (Block)" href="https://medschool.otago.ac.nz/course/view.php?id=1408">Psychological Medicine (Block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1406">Cardiovascular System</a></li>
            <li><a title="Endocrine" href="https://medschool.otago.ac.nz/course/view.php?id=1650">Endocrine</a></li>
            <li><a title="Gastrointestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1410">Gastrointestinal System</a></li>
            <li><a title="Metabolism" href="https://medschool.otago.ac.nz/course/view.php?id=1652"> Metabolism</a></li>
            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1409">Musculoskeletal System</a></li>
            <li><a title="Nervous System" href="https://medschool.otago.ac.nz/course/view.php?id=1653"> Nervous System</a></li>
            <li><a title="RCA" href="https://medschool.otago.ac.nz/course/view.php?id=1654">RCA</a></li>
            <li><a title="RDA" href="https://medschool.otago.ac.nz/course/view.php?id=1655">RDA</a></li>
            <li><a title="Renal" href="https://medschool.otago.ac.nz/course/view.php?id=1656">Renal</a></li>
            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1407">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 3 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1411">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1412">Cancer</a></li>
            <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1420">Clinical Pharmacology</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1413">EBP</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1414">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1415">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1416">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1417">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1418">Infection &amp; Immunology</a></li>
            <li><a title="Interprofessional Education" href="https://medschool.otago.ac.nz/course/view.php?id=1651">IPE</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1419">Pathology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1421">Professional Development</a></li>
            <li><a title="Psychological Medicine (Vertical)" href="https://medschool.otago.ac.nz/course/view.php?id=1422">Psychological Medicine (Vertical)</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1423">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1399">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1425">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1424">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2017 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1490">2017 HSFY</a></li>
            <li><a title="2018 ResearchSmart" href="https://medschool.otago.ac.nz/course/view.php?id=1471">2018 ResearchSmart</a></li>
            <li><a title="MBChB Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">MBChB Resources</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <!-- ALM4 Menu -->
  <div id="alm4" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
        <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 4 Navigation</h2></li>
            <li class="home"><a title="ALM 4 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1683">ALM 4 - Home</a><button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm4">Close X</button></li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">

          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>

            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1568">Addiction Medicine</a></li>   
              <li><a title="CardioRespiratory, Vascular, Plastic Surgery, Dermatology" href="https://medschool.otago.ac.nz/course/view.php?id=1573">CardioRespiratory, Vascular, Plastic Surgery, Dermatology</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1574">General Practice</a></li>
              <li><a title="Older Persons Health" href="https://medschool.otago.ac.nz/course/view.php?id=1575">Older Persons Health</a></li>
              <li><a title="Surgery / Emergency Medicine / Gastroenterology / Oncology" href="https://medschool.otago.ac.nz/course/view.php?id=1576">Surgery / Emergency Medicine / Gastroenterology / Oncology</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1577">Public Health</a></li>    

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1567">Clinical Skills</a></li>
              <li><a title="Ethics &amp; Law" href="https://medschool.otago.ac.nz/course/view.php?id=1569">Ethics &amp; Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1521">Hauora Māori</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1570">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1618">Pathology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1571">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1572">Quality and Safety</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Clinical Orientation 2019" href="https://medschool.otago.ac.nz/course/view.php?id=1611">Clinical Orientation 2019</a></li>
            </ul>
      
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Medicine 1" href="https://medschool.otago.ac.nz/course/view.php?id=1662">Medicine 1</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1672">Psychological Medicine</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1668">Public Health</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1666">Surgery</a></li>
              <li><a title="Urban GP / ENT" href="https://medschool.otago.ac.nz/course/view.php?id=1670">Urban General Practice / ENT</a></li>
              

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1665">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1700">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1688">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1691">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1689">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1695">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1699">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1682">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1698">Radiology</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Learning" href="https://medschool.otago.ac.nz/course/view.php?id=1697">Whole Class Learning</a></li>
              <li><a title="Pregnancy Long Case" href="https://medschool.otago.ac.nz/course/view.php?id=1694">Pregnancy Long Case</a></li>
              <li><a title="Paediatric Chronic Longitudinal Case" href="https://medschool.otago.ac.nz/course/view.php?id=1696">Paediatric Chronic Longitudinal Case</a></li>   
            </ul>

          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1581">Advanced Clinical Skills</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1540">General Practice</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1507">Public Health</a></li>
              <li><a title="Medicine and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1583">Medicine and Clinical Skills</a></li>
              <li><a title="Surgical and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1587">Surgery and Clinical Skills</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1582">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1585">Pathology</a></li>
              <li><a title="Professional Development and Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1586">Professional Development and Ethics</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1584">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1692">Virtual Clinical Skills</a></li>
            </ul>

          </ul>

          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li class="">
              <a href="https://medschool.otago.ac.nz/course/index.php?categoryid=183" title="Previous Years">2017-2018 ELM</a>
            </li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
  </div>
      </li>
    </ul>
  </div>


  <!-- ALM5 Menu -->
  <div id="alm5" class="collapse brnav-block">
    
    <!-- ALM 5 Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
            <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 5 Navigation</h2></li>
            <li class="home">
              <a title="ALM 5 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1684">ALM 5 - Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm5">Close X</button>
            </li>
            <li class="home_divider"></li>
          </ul>
            </div>
            <div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1554">Advanced Medicine</a></li>
              <li><a title="Orthopaedics and Advanced Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1555">Orthopaedics and Advanced Surgery</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1565">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1566">Psychological Medicine</a></li>
              <li><a title="Womens Health and Developmental Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1559">Womens Health and Developmental Medicine</a></li>  
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1558">Addiction Medicine</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1560">Clinical Skills</a></li>
              <li><a title="Ethics & Law" href="https://medschool.otago.ac.nz/course/view.php?id=1561">Ethics & Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1529">Hauora Māori</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1570">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1619">Pathology</a></li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1562">Clinical Pharmacology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1563">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1564">Quality and Safety</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Child Health & Reproductive Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1677">Child Health & Reproductive Medicine</a></li>
              <li><a title="Medicine 2" href="https://medschool.otago.ac.nz/course/view.php?id=1673">Medicine 2</a></li>
              <li><a title="Musculoskeletal, Anaesthesia and Intensive Care" href="https://medschool.otago.ac.nz/course/view.php?id=1676">Musculoskeletal, Anaesthesia and Intensive Care</a></li>
              <li><a title="Rural GP" href="https://medschool.otago.ac.nz/course/view.php?id=1617">Rural GP</a></li>
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1665">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1700">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1688">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1691">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1689">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1695">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1699">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1682">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1698">Radiology</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1701">Whole Class Learning</a></li>
              <li><a title="Paediatric Chronic Longitudinal Case" href="https://medschool.otago.ac.nz/course/view.php?id=1494">Paediatric Chronic Longitudinal Case</a></li>                       
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Primary Health Care and General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1589">Primary Health Care and General Practice</a></li>
              <li><a title="Musculoskeletal and Skin" href="https://medschool.otago.ac.nz/course/view.php?id=1591">Musculoskeletal and Skin</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1597">Psychological Medicine</a></li>
              <li><a title="General Medicine and Subspecialities" href="https://medschool.otago.ac.nz/course/view.php?id=1588">General Medicine and Subspecialities</a></li>
              <li><a title="Child and Adolescent Health" href="https://medschool.otago.ac.nz/course/view.php?id=1592">Child and Adolescent Health</a></li>
              <li><a title="Women’s Health" href="https://medschool.otago.ac.nz/course/view.php?id=1600">Women’s Health</a></li>                        
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1596">Clinical Pharmacology</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1590">Hauora Māori</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1598">Radiology</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1594">Pathology</a></li>
              <li><a title="Professional Development and Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1595">Professional Development and Ethics</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1593">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1692">Virtual Clinical Skills</a></li>

            </ul>
          </ul>
        </div>
        <div class="row-fluid">
          
          <ul class="unstyled span4 mylist">
            <li class="school-title">RMIP</li>
            <li><a title="Hauora Māori for RMIP" href="https://medschool.otago.ac.nz/course/view.php?id=1702">Hauora Māori for RMIP</a></li>
            <li><a title="RMIP Home" href="https://medschool.otago.ac.nz/course/view.php?id=1657">RMIP Home</a></li>
          </ul>
          
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=150" title="Previous Years">2016-2017 ELM</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=180" title="Previous Years">2018 ALM 4</a></li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
  </div>  
      </li>
    </ul>  
  </div>


  <!-- ALM6 Menu -->
  <div id="alm6" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
  <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 6 Navigation</h2></li>
            <li class="home">
        <a title="ALM 6 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1760">ALM 6 - Home</a>
        <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm6">Close X</button>
      </li>
            <li class="home_divider"></li>
          </ul>
  </div>
  <div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            <ul class="unstyled">

              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1747">Elective</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1749">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1750">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1748">Psychological Medicine</a></li>
              <li><a title="Critical Care" href="https://medschool.otago.ac.nz/course/view.php?id=1751">Critical Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1752">Surgery</a></li>
              <li><a title="Selective" href="https://medschool.otago.ac.nz/course/view.php?id=1753">Selective</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1754">General Practice</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1755">Medicine</a></li>
        
              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1739">Hauora Māori</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1757">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1758">Professional Development</a></li>
              <li><a title="Friday Afternoon Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1759">Friday Afternoon Teaching</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1756">Clinical Skills</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1843">Elective</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1848">Obstetrics & Gynaecology</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1815">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1816">Psychological Medicine</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1845">Medicine</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1844">Surgery</a></li>
              <li><a title="Community – Evaluation – Outpatients" href="https://medschool.otago.ac.nz/course/view.php?id=1817">Community – Evaluation – Outpatients</a></li>
              <li><a title="Critical Care and Emergency Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1818">Critical Care and Emergency Medicine</a></li>
              
              <li><br /></li>
              <li class="programme">Vertical Modules</li>
        <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1665">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1700">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1688">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1691">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1689">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1695">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1699">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1682">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1698">Radiology</a></li>
              <li><br /></li>

              <!--<li class="programme">Other</li>-->
              <!--<li><a title="2015d5 Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=771">Whole Class Teaching</a></li>-->
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1802">Elective</a></li>
              <li><a title="Primary Health Care & General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1803">Primary Health Care & General Practice</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1804">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1805">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Emergency Medicine / Acute Care" href="https://medschool.otago.ac.nz/course/view.php?id=1806">Emergency Medicine / Acute Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1807">Surgery</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1808">Medicine</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1809">Psychological Medicine</a></li>


              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Professional Development & Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1810">Professional Development & Ethics</a></li>
              <li><a title="Palliative Medicine & End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1811">Palliative Medicine & End of Life Care</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1812">Virtual Clinical Skills</a></li>

              <li><br /></li>        

              <!--<li class="programme">Other</li>-->
            </ul>
          </ul>
  </div>
  <div class="row-fluid">        
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title-sm">Cross-campus</li>
            <li><a title="TIs as Teachers" href="https://medschool.otago.ac.nz/course/view.php?id=1687">TIs as Teachers</a></li>
            <li><a title="Prepared for Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1333">Prepared for Practice</a></li>
          </ul>
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=128" title="2015-6 ELM">2015-2016 ELM</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=151" title="2017 ALM4">2017 ALM 4</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=173" title="2018 ALM5">2018 ALM 5</a></li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
  </div>          
  
      </li>
    </ul>  
  </div>
