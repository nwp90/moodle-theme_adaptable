<?php global $OUTPUT; ?>
    <!-- Custom Navbar Inserted -->
    <div class="pull-left">
      <!-- <a class="brand" href="#"></a> -->
      <div id="bootbutt"class="  panel">
        <!-- BR Nav -->
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#nurs" data-parent="#bootbutt">
          PGNS
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#pubh" data-parent="#bootbutt">
          PUBH
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#radt" data-parent="#bootbutt">
          RADT
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#rtru" data-parent="#bootbutt">
          RTRU
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#gena" data-parent="#bootbutt">
          GENA
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#psyc" data-parent="#bootbutt">
          PSYC
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#avmed" data-parent="#bootbutt">
          AVMED
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#spme" data-parent="#bootbutt">
          SPME
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#other" data-parent="#bootbutt">
          Other
        </button>
        <button type="button" class="btn collapsed bbnav" data-toggle="collapse" data-target="#helpmenu">
          Help
        </button>   
        <!-- BR Nav END -->
      </div>
    </div>
