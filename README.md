The 'medadaptable' branch is our local version of adaptable. Although you may find
useful ideas in here, it's not intended directly for use by anyone else. Look at
the 'sane' branch for that.
